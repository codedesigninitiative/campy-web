<div class="mt-4 md:mt-0 w-full md:w-1/2 xl:w-1/3">
    <div class="flex flex-col justify-between / mr-3 / h-full / border border-1 border-gray-200 / rounded">
        <div>
            <a href="{{ $blog->getPath() }}"><img src="{{ $blog->image }}" alt="{{ $blog->caption }}" class="rounded" /></a>
            <h2 class="ml-2 mt-2"><a href="{{ $blog->getPath() }}">{{$blog->caption}}</a></h2>
            <div class="ml-2 mb-4 text-sm text-gray-600">
                {{ $blog->author }} | {{ date('d-m-Y', $blog->published_at) }}
            </div>
        </div>
        <p class="flex-1 mx-2 mb-4 text-lg leading-normal">{{ $blog->lead }}</p>
        <div class="ml-2 mb-2">
            @component('_components.blog.read_more')
                @slot('url', $blog->getPath())
            @endcomponent
        </div>
    </div>
</div>
