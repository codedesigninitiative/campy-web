<div class="p-1 w-full md:w-1/2 xl:w-1/3">
    <div class="bg-white p-4 rounded h-full text-center">
        <div>
            <img src="{{ $magazin->img }}" alt="{{ $magazin->title }}" />
            <div class="text-2xl back mb-1 mt-4">
                {{ $magazin->title }}
            </div>
            <p class="text-lg mt-4 leading-normal">{{ $magazin->slot }}</p>
        </div>
        <div class="mb-4 mt-8">
            @component('_components.button.download')
                @slot('url', $magazin->pdf)
            @endcomponent
        </div>
    </div>
</div>