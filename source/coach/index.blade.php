@extends('_layouts.master')

@section('body')
            <div class="lg:flex text-lg">
            <div class="flex-1 lg:ml-4">
                <a class="underline" href="/coach">🇩🇪German</a>
                <a class="underline" href="/en/coach">🇬🇧English</a>
                <div class="mt-4">
                    <h3>Worum geht's?</h3>
                    <ul class="list-disc pl-4">
                        <li>Auf den Code+Design Camps arbeiten 50-75 Jugendlichen an Projektideen, die sie selbst mitbringen</li>
                        <li>Ziel der Camps ist es, Jugendliche ohne und mit Vorerfahrung für die IT-Berufe zu begeistern</li>
                    </ul>
                </div>

                <div class="mt-4">
                    <h3>Was macht ein Coach?</h3>
                    <p>Coaches auf den Code+Design Camps…</p>
                    <ul class="list-disc pl-4">

                        <li><em>begleiten</em> Jugendliche bei ihren Projekten im Bereich Web, App, Gaming, Hardware und/oder Design</li>
                        <li>helfen bei <em>Teambildung und Projektmanagement</em></li>
                        <li><em>inspirieren</em> als Praktiker aus der Tech-Branche – egal ob als Profi oder Student, Angestellter oder Selbstständiger und stehen (optional) bei der Berufsorientierung als Gesprächspartner zur Verfügung</li>
                        <li><em>organisieren</em> (optional) praktische Workshops für Anfänger und Fortgeschrittene durch</li>
                    </ul>
                </div>

                <div class="mt-4">
                    <h3>Wie kann ich als Coach unterstützen?</h3>
                    <ul class="list-disc pl-4">
                        <li>Du kannst in 2 Bereichen coachen: inhaltlich und/oder teambezogen.</li>
                        <li>Inhalt: Du hilfst bei technischen Fragen und bietest vielleicht auch einen Workshop an. </li>
                        <li>Team: Du bist Ansprechpartner für 1-2 Teams und hilfst ihnen, ihre Ziele zu erreichen</li>
                        <li>Als Coach solltest du mindestens 1 Tag dabei sein</li>
                    </ul>
                </div>

                <div class="mt-4">
                    <h3>Was passiert (aus Coach-Sicht) auf den Camps?</h3>
                    <ul class="list-disc pl-4">
                        <li><em>1. Tag</em>: Begeistern und Beraten: Du begeisterst Jugendliche für Tech, indem du ihnen präsentierst, was du gerne machst (Open Source, Firmenprojekte…). Du unterstützt als Coach Teams dabei, die für sie richtige Technologie zu wählen und Lernmaterial zu finden</li>
                        <li><em>Folgetage</em>: Die Coaches bieten pro Camptag je 1,5h Sprechstunden bei Problemen in den Projekten (Technologie, Lernen, Team) an. Außerdem organisieren sie interaktive Workshops à 1,5h zu Themen im Bereich Web, Mobile, Games.</li>
                    </ul>
                </div>

                <div class="mt-4">
                    <div class="mt-4 mb-2">
                        <h3>Mehr Infos</h3>
                        <a class="underline" href="/coach/faq">Fragen und Antworten</a> |
                        <a class="underline" href="/initiative/philosophie">Philosophie von Code+Design</a>
                    </div>
                </div>

                <h2>Interesse anzeigen</h2>
                <div class="flex">
                    <a class="underline" href="/kontakt" target="_blank">Kontaktiere uns, wenn du als Coach dabeisein willst.</a>
                </div>

            </div>
        </div>
@endsection

@section('title')
Sei als Coach dabei
@endsection
