---
active: no
cancelled: yes
type: Meetup
city: Berlin
color: hsl(23, 100%, 50%)
color_secondary: gray
date_start: 2019-07-06
date_end: 2019-07-06
days: 1
deadline: 2019-07-06
extends: _layouts.events
section: content
# headerimage: header01.jpg
id: 1907-ber
event_id: 23
location: Lohmühlenstr. 65, 12435 Berlin
locationlink: 'https://factoryberlin.com'
slug: berlin
time_start: '10:00'
time_end: '18:00'
youtube: xGk1PpIbisU
meals: Snacks
cost: 0€
costlaptop: 10€
laptopfree: no
teaser: no
partners: no
supporters: no
---
<p class="text-lg hyphens mb-6 leading-normal">
  Beim Code+Design Meetup gibt es Workshops, Develop Your Future mit Leuten aus der Praxis, eigene Projekte und viele nette digitale Leute.
</p>
