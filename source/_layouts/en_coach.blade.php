@extends('_layouts.master')

@section('body')
    <div class="text-white text-3xl mb-4">{{ $page->title }}</div>

        <div class="flex-1 lg:ml-4 markdown">
            @yield('content')
        </div>
    <style>
        .markdown h3 {
            margin-top: 1rem;
        }
    </style>
@endsection
