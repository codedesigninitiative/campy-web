@extends('_layouts.master')

@section('body')
        <div class="flex flex-wrap">
            @foreach ($testimonials as $testimonial)
                <div class="w-full md:w-1/2 p-1">
                    @component('_components.card')
                        @slot('src') {{ $testimonial->src }} @endslot
                        @slot('url') {{ $testimonial->resource }} @endslot
                        @slot('medium') {{ $testimonial->medium }} @endslot
                        @slot('channel') {{ $testimonial->channel }} @endslot
                        @slot('published_at') {{ date('d.m.y', $testimonial->published_at) }} @endslot

                        @yield('content')
                        {{ $testimonial->content }}
                    @endcomponent
                </div>
            @endforeach
        </div>

@endsection

@section('title')
Stimmen zu Code+Design
@endsection
