@extends('_layouts.master')

@section('body')

    <h1>Karriere@Code+Design</h1>

        <div id="start">
            @include('_partials.events.header')
            <h1 class="text-2xl font-bold" id="stellenanzeigeeventundprojektmanagercd">Jobangebot: Event- und Projektmanager*in</h1>

            <ul class="list-disc">
                <li>Projektmanager (m/w/d) für das Projekt</li>

                <li><em>Code+Design, Jugendliche für Digitales begeistern</em></li>

                <li>Vollzeit, Teilzeit, unbefristet</li>
                <li>ab sofort (15.10.2019)</li>
            </ul>

            <p class="mt-4 underline text-sm text-gray-700">
                <a href="#bewerbungsformular" class="">(Direkt zum Bewerbungsformular)</a> | <a href="#privacy">(Datenschutzerklärung)</a>
            </p>

            <h2 class="mt-8 mb-2 text-xl font-bold" id="waswirmachen">Was wir machen</h2>

            <ul class="list-disc">
                <li>Wir sind ein gemeinnütziger Verein, der sich selbst finanziert</li>

                <li><strong>Unsere</strong> <strong>Mission</strong>: Wir begeistern Jugendliche für die digitalen
                    Berufe – vor allem junge Frauen und Jugendliche, die noch gar keinen
                    Kontakt mit der digitalen Welt haben</li>

                <li>Wir organisieren dafür 2020 mindestens vier Code+Design Camps mit je 60
                    Jugendlichen in Berlin (jeweils zwischen 2 und 4 Tagen)</li>

                <li>Darüber hinaus unterstützen wir weitere Events in ganz Deutschland, die in Zusammenarbeit mit lokalen Partnern durchgeführt werden</li>

                <li>Auf den Camps haben die TeilnehmerInnen die Chance, mit Hilfe von
                    Coaches eigene digitale Projekte umzusetzen, neue Freunde zu treffen
                    und die Arbeitswelt der Zukunft reinzuschnuppern. </li>
            </ul>

            <h2 class="mt-8 mb-2 text-xl font-bold" id="wasdieaufgabensind">Was die Aufgaben sind</h2>

            <ul class="list-disc">
                <li>Hauptverantwortung der Organisation und Durchführung von mindestens
                    4 großen Camps pro Jahr in Berlin</li>

                <li>Einsatz in Berlin während der eigenen Camps</li>

                <li>Betreuung von bestehenden Partnerschaften mit Schulen und Sponsoren,
                    Akquise von neuen</li>

                <li>Koordination von 3 studentischen Vorständen</li>

                <li>Aktive Einbindung des Ehrenamtes (Kuratorium, Freiwillige)</li>

                <li>Koordination von externen Partnern für die Vereinsorganisation:
                    (Lohn-)Buchhalter, Steuerberater</li>

                <li>Unterstützung bei der Betreuung der Social-Media-Kanäle des Projekts</li>

                <li>Betreuung der Community bei der Vorbereitung und Durchführung
                    von selbstorganisierten Events (Camps, Meetups)</li>

                <li>Mitarbeit beim Aufbau einer Alumni-Community</li>
            </ul>

            <h2 class="mt-8 mb-2 text-xl font-bold" id="wenwirsuchen">Wen wir suchen</h2>

            <ul class="list-disc">
                <li>Du findest unsere Mission richtig gut</li>

                <li>Du hast selbst schon Events organisiert und durchgeführt</li>

                <li>Du hast bereits eigene Projekte selbstständig und erfolgreich
                    durchgeführt</li>

                <li>Du bist halb Improvisationstalent, halb Ordnungsfanatiker/in</li>

                <li>Du arbeitest dich gerne und schnell in Software und digitale Tools
                    ein</li>

                <li>Du erkennst und löst kleine und große Probleme eigenständig, schnell
                    und pragmatisch</li>

                <li>Du bist freundlich, hilfsbereit und organisiert dich selbst sehr gut</li>

                <li>Du arbeitest sowohl gut autonom als auch im Team (du kannst Hilfe
                    anbieten und um Hilfe bitten)</li>

                <li>Du arbeitest gerne mit Jugendlichen und jungen Erwachsenen </li>
            </ul>

            <h2 class="mt-8 mb-2 text-xl font-bold" id="wasdichbeiunserwartet">Was dich bei uns erwartet</h2>

            <ul class="list-disc">
                <li>Du bist die treibende Kraft hinter unserer Initiative und als
                    einzige Vollzeitkraft alleiniger Ansprechpartner.</li>

                <li>Zur Seite stehen dir 3 studentische Vorstandsmitglieder, die dich
                    jeweils mit 10 Stunden / Woche unterstützen.</li>

                <li>Du arbeitest vom CODE Campus aus und bist damit perfekt integriert
                    in die Startup-Welt: Auf dem Campus lernst du Tür an Tür mit
                    inspirierenden Gründern und Gründerinnen und erfolgreichen
                    Unternehmerinnen und Unternehmern der Tech- und Startupszene. </li>

                <li>Du arbeitest mit engagierten Jugendlichen und jungen Erwachsenen
                    (sie sind der Kern des Vereins, wir binden sie überall mit ein!)</li>

                <li>Du bist auf allen 4 Camps dabei, die auch am Wochenende stattfinden;
                    du kannst diese Stunden direkt nach den Events an Wochentagen
                    natürlich ausgleichen</li>

                <li>Du kannst dich aufs Projekt- und Eventmanagement konzentrieren –
                    Adminaufgaben (Buchhaltung etc.) sind entweder schon an externe
                    Partner abgegeben oder sollen noch outgesourct werden</li>
            </ul>

            <h2 class="mt-8 mb-2 text-xl font-bold" id="wiediebewerbungabluft">Wie die Bewerbung abläuft</h2>

            <ul class="list-disc">
                <li>Schritt 1: Du füllst das <a href="#bewerbungsformular" class="text-primary-700 underline">Bewerbungsformular</a> aus – mit deinem maximal 2-seitigen Lebenslauf als
                    PDF und deinen Antworten auf unsere 3 Fragen, warum du für die Stelle
                    geeignet bist (Wir brauchen kein formelles Anschreiben und keine
                    Zeugnisse)</li>

                <li>Schritt 2: Wir nehmen uns 2 Wochen Zeit, deine Bewerbung ausführlich
                    zu lesen und melden uns bei dir zurück</li>

                <li>Schritt 3: Kommst du in die nähere Auswahl, laden wir dich zu einem
                    Interview ein.</li>

                <li>Schritt 4: Die Vorbereitung und Durchführung von Camps ist unser
                    Kerngeschäft. Deshalb laden wir maximal 3 Personen zu einem
                    bezahlten Probearbeiten ein, bei dem das erste Camp 2020 geplant
                    wird</li>
            </ul>

            <h2 class="mt-8 mb-2 text-xl font-bold" id="welcheantwortenzudeinemmglicheneinsatzbeiunsunsinteressieren">Welche Antworten zu deinem möglichen Einsatz bei uns uns interessieren</h2>

            <ul class="list-disc">
                <li>Warum willst Du für die Code+Design Initiative e.V. arbeiten (in
                    einem Satz)?</li>

                <li>Du musst alleine ein Camp für 100 Jugendliche organisieren, das in 2
                    Wochen beginnt. Was sind heute deine 5 wichtigsten To Dos?</li>

                <li>Wobei bräuchtest du bei diesem Job Unterstützung?</li>
            </ul>
        </div>
        <h2 class="mt-8 mb-2 text-2xl font-bold" id="bewerbungsformular">Bewerbungsformular</h2>
        <iframe class="mt-8 airtable-embed" src="https://airtable.com/embed/shrMwJE2zdYo8LHSj?backgroundColor=purple" frameborder="0" onmousewheel="" width="100%" height="2500" style="background: transparent; border: 1px solid #ccc;"></iframe>

        <div id="privacy" class="mt-8">
            <h1 class="text-2xl font-bold">Datenschutzerklärung</h1>

            <p class="mt-4"> Der Schutz deiner personenbezogenen Daten bei der Bearbeitung während des gesamten Bewerbungsprozesses ist für uns ein wichtiges Anliegen.</p>

            <p class="mt-4"> Wir sind die verantwortliche Stelle im datenschutzrechtlichen Sinne. Es gilt deutsches Datenschutzrecht.</p>

            <p class="mt-4"> Bitte lese dir die folgenden Informationen sorgfältig durch. Indem du dir bei deinem Bewerberformular im Anschluss ein Häkchen bei der Zustimmung zur Datenschutzerklärung setzst, erklärst du dich mit der hier dargestellten Erhebung, Verarbeitung und Nutzung deiner personenbezogenen Daten durch unseren Verein einverstanden.</p>

            <p class="mt-4"> Solltest Sie der vorgenannten Datenschutzerklärung nicht zustimmen, so ist es leider nicht möglich, dass du dich über diese Karriereseite elektronisch bei uns bewirst.</p>

            <h2 class="mt-4 text-xl font-bold">Ansprechpartner bei Fragen oder Auskunftsersuchen</h2>

            <p class=""> Solltest die Fragen oder Anregungen in Bezug auf den Datenschutz oder die Verarbeitung Ihrer persönlichen Daten haben, so stehen wir dir unter hello@code.design zur Verfügung.</p>

            <h2 class="mt-4 text-xl font-bold">Personenbezogene Daten und Zweckbindung</h2>

            <p class=""> Gegenstand des Datenschutzes sind personenbezogene Daten (Art. 4 Nr. 1 DSGVO). Hierbei handelt es sich um Einzelangaben über persönliche oder sachliche Verhältnisse, wie z. B. Name, Adresse, E-Mail-Adresse oder Telefonnummer, die Sie uns im Rahmen des Bewerbungsverfahrens zu Verfügung stellen:.</p>

            <ul class="list-disc">
                <li>Personenstammdaten (z.B. Bewerbername, Adresse, Geburtsdatum)</li>
                <li>Kommunikationsdaten (z.B. Telefon, E-Mail)</li>
                <li>Daten des Bewerberlebenslaufes (z.B. Lebenslauf)</li>
                <li>Ergebnisse von Auswahlverfahren (z.B. Tests, Interviews)</li>
                <li>Prozessdaten (z.B. Status, Termine)</li>
            </ul>

            <p class="mt-4"> Deine personenbezogenen Daten werden ausschließlich zum Zweck der Bewerbungsabwicklung elektronisch gespeichert und genutzt.</p>

            <h2 class="mt-4 text-xl font-bold">Rechtliche Grundlage</h2>

            <p class=""> Die Verarbeitung Ihrer personengebundenen Daten ist gemäß Art. 6 Abs. 1 lit. a. DSGVO rechtmäßig, da du vor Abschluss der Bewerbung deine Einwilligung zur Verarbeitung Ihrer personenbezogenen Daten für einen oder mehrere bestimmte Zwecke erteilt hast.</p>

            <h2 class="mt-4 text-xl font-bold">Einwilligung zur Verarbeitung und Nutzung</h2>

            <p class=""> Du erklärst sich damit einverstanden, dass während des Bewerbungsprozesses der Vorstand und das Kuratorium Zugriff auf deine personenbezogenen Daten erhalten. Deine Daten werden ausschließlich von einem eingegrenzten Benutzerkreis verwendet. Deine persönlichen Daten werden in keiner Form von uns oder durch uns beauftragte Personen an Dritte weitergegeben, außer wir sind zur Weitergabe aufgrund zwingender gesetzlicher Regelungen verpflichtet (z.B. an staatliche Einrichtungen).</p>

            <h2 class="mt-4 text-xl font-bold">Auftragsdatenverarbeitung</h2>

            <p class=""> Aufgrund einer gesonderten Vereinbarung über die Verarbeitung von personenbezogenen Daten werden Ihre personenbezogenen Daten von der Firma <a href="https://airtable.com/privacy" target="_blank" class="underline"> Airtable, 799 Market St, San Francisco</a> im Rahmen einer Auftragsdatenverarbeitung nach Art. 28 DSGVO gemäß den entsprechenden gesetzlichen Vorgaben in unserem Auftrag erhoben, verarbeitet und genutzt. Hiermit ist auch keine Übermittlung deiner persönlichen Daten an Dritte im datenschutzrechtlichen Sinne verbunden.<br>
                Wir bleiben Ihnen gegenüber datenschutzrechtlich verantwortlich.</p>

            <h2 class="mt-4 text-xl font-bold">Aufbewahrung und Löschung der Daten</h2>

            <p class=""> Es gelten die allgemeinen gesetzlichen Aufbewahrungs- und Löschfristen. Wir löschen Ihre Daten gemäß den einschlägigen gesetzlichen Bestimmungen nach Abschluss des jeweiligen Stellenbesetzungsverfahrens.</p>

            <p class="mt-4"> Ein Zugriff und eine Verwendung Ihrer persönlichen Daten seitens unseres Unternehmens ist dann nicht mehr möglich.</p>

            <h2 class="mt-4 text-xl font-bold">Datenübermittlung in Drittländer</h2>

            <p class=""> Eine Datenübermittlung findet in Einklang mit der DSGVO in den Drittstatt USA statt.</p>

            <h2 class="mt-4 text-xl font-bold">Zugriffs- und Berichtigungsrecht, Recht auf Widerruf</h2>

            <p class=""> Du hast das Recht, die von dir zur Verfügung gestellten personenbezogenen Daten jederzeit durch Mitteilung an den Empfänger zu aktualisieren oder die Löschung zu verlangen. Du kannst selbst bestimmen, welche Informationen du uns zur Verfügung stellst. Mit einem * gekennzeichnete Felder sind Pflichtangaben, die wir für das Bewerbungsverfahren benötigen. Nicht mit einem * gekennzeichnete Felder können von dir freiwillig ausgefüllt werden. Für alle Inhalte Ihrer Onlinebewerbung, wie z. B. Fotos, bist du selbst verantwortlich und musst eigenverantwortlich auf die Einhaltung gesetzlicher Vorgaben, wie z. B. Marken-, Urheber-, Persönlichkeits- oder sonstige Rechte Dritter, achten.</p>

            <p class="mt-4"> Du hast das Recht auf Datenübertragbarkeit und das Recht auf Beschwerde bei einer Datenschutzaufsichtsbehörde.</p>

            <p class="mt-4"> Du bist jederzeit berechtigt, die Einwilligung in die Verwendung personenbezogener Daten zu widerrufen.</p>

            <h2 class="mt-4 text-xl font-bold">Pflicht zur Datenbereitstellung</h2>

            <p class=""> Sofern du uns die im vorherigen Kapitel beschriebenen Pflichtangaben nicht zur Verfügung stellst, so ist es leider nicht möglich, dass du dich über diese Karriereseite elektronisch bei uns bewirbst.</p>

            <h2 class="mt-4 mt-4 text-xl font-bold">Sicherheit</h2>

            <p class=""> Datenschutz und Datensicherheit haben für uns oberste Priorität. Daher setzen wir verschiedene technische und organisatorische Sicherheitsmaßnahmen ein, um deine Daten vor zufälliger oder vorsätzlicher Manipulation, Verlust, Zerstörung oder Zugriff unberechtigter Dritter zu schützen.</p>

            <p class="mt-4"> Im Falle der Erhebung und Verarbeitung persönlicher Daten im genannten Rahmen werden diese Informationen in verschlüsselter Form übertragen, um stets die ausreichende Sicherheit deiner Daten zu gewährleisten. Die Maßnahmen passen wir laufend an die technische Entwicklung an, um größtmögliche Sicherheit auch in Zukunft gewährleisten zu können.</p>

            <h2 class="text-xl font-bold">Einsatz von Cookies</h2>

            <p class=""> Im Rahmen deiner Onlinebewerbung werden Cookies eingesetzt. Durch den Einsatz dieser Cookies werden weder persönliche Daten gespeichert noch solche mit Ihren persönlichen Nutzerdaten verbunden. Cookies sind kleine Textdateien, die von unserem Webserver an deinen PC verschickt und dort meist auf Ihrer Festplatte abgespeichert werden. Sie werden nicht Bestandteil deines Systems und können auch keinen Schaden anrichten. Die meisten Browser sind so eingestellt, dass sie Cookies automatisch akzeptieren.</p>
        </div>

@endsection

@section('title')
Karriere@Code+Design
@endsection

@section('styles')
iframe.quieter {
display:none;
}
@endsection
