---
extends: _layouts.partners
name: telekom
logo: telekom.svg
tier: partner
strength: 4000
events: bon1808,due1810,1907-bon
short:
height: h-16
website: https://www.telekom.com/de
---
