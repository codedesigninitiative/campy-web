<div class="block bg-white p-8 text-lg sm:rounded-t">
    <div class="flex flex-wrap mb-4">
        <div class="w-full md:w-1/2 lg:w-1/4 h-auto p-2">
            <h3>Code+Design</h3>
            <ul class="list-none p-0 leading-normal">
                <li class="hover:text-primary-500 text-gray-800"><a href="/initiative" class=" hover:text-primary-500 text-gray-800" class=" hover:text-primary-500 text-gray-800">Initiative</a></li>
                <li class="hover:text-primary-500 text-gray-800"><a href="/initiative/partner" class=" hover:text-primary-500 text-gray-800" class=" hover:text-primary-500 text-gray-800">Partner</a></li>
                <li class="hover:text-primary-500 text-gray-800"><a href="/initiative/stimmen" class=" hover:text-primary-500 text-gray-800">Stimmen</a></li>
                <li class="hover:text-primary-400 text-gray-800"><a href="/initiative/philosophie" class="text-gray-800  hover:text-primary-500">Philosophie</a></li>
                <li class="hover:text-primary-500 text-gray-800"><a href="/kontakt" class=" hover:text-primary-500 text-gray-800">Kontakt</a></li>
                <li class="hover:text-primary-500 text-gray-800"><a href="/impressum" class=" hover:text-primary-500 text-gray-800">Impressum</a></li>
                <li class="hover:text-primary-500 text-gray-800"><a href="/datenschutz" class=" hover:text-primary-500 text-gray-800">Datenschutz</a></li>
            </ul>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/4 h-auto p-2">
            <h3>Events</h3>

            <ul class="list-none p-0 leading-normal">
                @forelse ($events->where('active', 'yes') as $event)
                    <li class="hover:text-primary-500 text-gray-800"><a href="{{  $event->getPath() }}" class=" hover:text-primary-500 text-gray-800">{{  $event->city }}, @if ($event->days > 1){{ date('d.m.y', $event->date_start) }}-{{ date('d.m.y', $event->date_end) }}@else {{ date('d.m.y', $event->date_start) }} @endif </a></li>
                @empty
                    <li class="text-gray-800">Keine aktiven Events</li>
                @endforelse

                <li class="hover:text-primary-400 text-gray-800 mt-4"><a href="/events/#past_events" class=" hover:text-primary-500 text-gray-800">Alle vergangenen Events</a></li>

                <li class="hover:text-primary-500 text-gray-800"><a href="/teilnahmebedingungen" class=" hover:text-primary-500 text-gray-800">Teilnahmebedingungen</a></li>

            </ul>
        </div>

            <div class="w-full md:w-1/2 lg:w-1/4 h-auto p-2">
                <h3><a href="/magazin" class="">Magazin</a></h3>
            <div class="mt-6">
                <h3><a href="/initiative/unterstuetzen" class="">Unterstützen</a></h3>
                <ul class="list-none p-0 leading-normal mt-2">
                    <li class="text-gray-800"><a href="/coach" class=" hover:text-primary-600 text-gray-800">Coach auf Event</a></li>
                    <li class="text-gray-800"><a href="/initiative/unterstuetzen" class=" hover:text-primary-600 text-gray-800">Spenden</a></li>
                    <li class="text-gray-800"><a href="/initiative/unterstuetzen" class=" hover:text-primary-600 text-gray-800">Sponsoring</a></li>
                    <li class="text-gray-800"><a href="/kontakt" class=" hover:text-primary-600 text-gray-800">Ehrenamtlich tätig werden</a></li>
                </ul>
            </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/4 p-2 h-auto">
            <div>
            <h3>Newsletter</h3>
                <div class="-ml-4">
                    @include('_partials.newsletter')<br>
                </div>
            </div>
            <div class="mt-4 flex" id="social">
                <a href="https://instagram.com/codeunddesign/" target="_blank" class="mr-2"><svg class="stroke-current text-gray-500 hover:text-primary-500 w-8 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.5" y2="6.5"></line></svg></a>

                <a href="https://www.youtube.com/channel/UCuT3xJjPZFqQEEpleHBxVuA/videos" target="_blank"><svg class="stroke-current text-gray-500 hover:text-primary-500 w-8 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-play-circle"><circle cx="12" cy="12" r="10"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg></a>

                <a href="https://www.facebook.com/codeunddesign/" target="_blank"><svg class="stroke-current text-gray-500 hover:text-primary-500 w-8 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a>
            </div>
        </div>

    </div>
</div>
<div class="container mx-auto bg-gray-200 text-gray-700 p-8 leading-normal text-center">
    <p>Die Code+Design Initiative e.V. ist ein gemeinnütziger Verein zur Berufs- und Studienorientierung im Bereich Informatik und Design.</p>
    <p>Vereinsregister VR 35667 B | Code+Design Initiative e.V., Lohmühlenstr. 65, 12435 Berlin</p>
</div>
