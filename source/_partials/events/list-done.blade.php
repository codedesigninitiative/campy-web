@component('_components.events.row')
    @slot('title', 'Die letzten 10 Events')
    @slot('events', $events->where('active', 'no')->whereNotIn('cancelled', ['yes'])->sortByDesc('date_start')->take(10))
@endcomponent