
<div class="md:flex items-center">
    <div class="md:w-3/5">
        <div class="text-2xl mb-4 mt-8">Was ist das C+D Camp?</div>
        <p class="text-lg leading-normal">Ein Code+Design Camp ist eine sehr gute Möglichkeit für junge Interessierte (14-22 Jahre) einen genaueren Einblick in die Berufswelt Code und Design zu bekommen. Nicht nur Neulinge, sondern auch schon Fortgeschrittene kommen hierbei auf ihre Kosten in einem Camp voller neuer Erfahrungen, sozialer Kontakte, fachlicher Kompetenz und einer Menge Spaß. Ob man sich nun mit programmieren oder designen beschäftigt ist dir natürlich selbst überlassen. Oder hast du doch eher Lust auf ein Hardware-Projekt?</p>
        
        <div class="mt-8">
            @include('_partials.events.skills')
        </div>

    </div>
    <div class="md:w-2/5 md:ml-4 mt-4 mb-4 md:mt-0">
        <div id="plyr-youtube" class="w-full" data-type="youtube" data-video-id="0o-zf_nULUQ"></div>
    </div>
</div>