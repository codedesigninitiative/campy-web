<nav class="md:flex items-center justify-between flex-wrap bg-white px-6 py-2 sm:rounded-b">
    <div class="flex items-center justify-center md:justify-start flex-shrink-0 text-black mr-6">
        <a href="/">
            <svg class="fill-current h-20 w-auto {{ $class ?? '' }} font-lemonmilk" xmlns="http://www.w3.org/2000/svg" id="Ebene_1" data-name="Ebene 1" viewBox="0 0 841.35 398.07"><defs><style>.cls-1{font-size:12px;}.cls-1,.cls-2,.cls-3,.cls-4,.cls-5{font-family:LemonMilk, "Lemon/Milk";}.cls-2{font-size:20px;}.cls-3{font-size:30px;}.cls-4{font-size:16px;}.cls-5{font-size:144.53px;}.cls-6{letter-spacing:-0.06em;}.cls-7{letter-spacing:0em;}.cls-8{font-size:251.31px;font-family:LemonMilkLight;}.cls-9{font-family:LemonMilkLight;}</style></defs><title>Logo CD black #2</title><path d="M276.57,618.61c-.83,2.71-17.1,2.34-31.83-1.67-6.6-1.79-13-4-14.46-1.86s3.19,6,2.16,7.77c-2.82,4.92-44.7-11.53-43.5-15.83,1.53-5.45,71.11,12.76,73.84,4.47.86-2.61-5.72-7.3-14-12a135.27,135.27,0,0,1,6.8-60.66c3.85-10.89,6.29-13.26,11.35-25.36A195.45,195.45,0,0,0,276.11,486a15.5,15.5,0,0,1-16.73-.87c1.9.12,21.08,1.12,31.13-12.75a29.11,29.11,0,0,0,5-12.18,26.42,26.42,0,0,1-17,11.67c2.59-1.51,10.07-6.32,13.26-15.46a30.75,30.75,0,0,0,1.53-11.22,27.13,27.13,0,0,0-2-9.86,25.52,25.52,0,0,0-6-8.47,203.63,203.63,0,0,0-21.46-17.6l-7.62,9.32c1.18,1,11.05,9.25,15.64,14.87a11.11,11.11,0,0,1,2.38,4.56,11.53,11.53,0,0,1-.43,6.3c-1.89,5.93-7.17,9.64-10,11.31-5.74,3.38-9.89,3.3-14.76,4.54-12,3-15.1,9.94-25.54,17.23-6.22,4.33-16.24,9.26-32.23,10.28,14,15.48,4.34,34.78,18.07,50,10.85-8.48,13.8-10.51,14-10.32s-4.76,4.34-11.22,11.92c-1.1,1.3-2,2.42-2.68,3.26a22.52,22.52,0,0,1-5.88-6.16c-3.65-5.72-6.26-15.87-6.7-22-.06-.9-.2-3.4-1.86-4.46a4,4,0,0,0-2.6-.43,63.39,63.39,0,0,0-40.57,22.38,77.54,77.54,0,0,0-9.41,17.41,95.74,95.74,0,0,0-4.45,13.92c-.42,1.75-.74,3.36-1,4.82l13.64-18c.51,5.62.6,9.81,1.32,13.89a46.21,46.21,0,0,1-.45,19.42c-.84,3.32-2.25,6.25-5.13,12.1-1.23,2.49-2.29,4.41-4.44,5.58-1.72.94-2.61.58-5,1.38-1.07.37-6.31,2.17-6.36,4.43-.06,2.82,8.58,5.24,10.58,5.8a60.62,60.62,0,0,0,19.91,2.2h0c54.13-11.26,67.87-12.62,68.25-11.1C230,606.47,201,619.25,168.07,620c-7.93.2-24.39.59-25.07-3-.85-4.5,23.22-16.63,32.51-9.22,5.85,4.67,1.17,13.38,6.65,17.57,3.42,2.61,8.8,1.86,47.11-14.39,6.16-2.62,13.64-5.83,24.1-6.83,9.56-.9,18.27.35,18.36,1.62s-8.71,2.55-8.56,4c.06.68,2,.8,4.29,1.55C272.28,613,277.06,617,276.57,618.61ZM222,599.13h-52c2.87-3.58,5.77-6.47,7.79-9.89,2.11-3.59,3.3-7.71,5.06-11.51.59-1.28,1.71-3.27,2.61-3.29,13.09-.21,26.18-.14,39.92-.14C224.13,582.89,223.06,590.88,222,599.13Z" transform="translate(-130.5 -314.02)"/><path d="M753.18,490.47" transform="translate(-130.5 -314.02)"/><path d="M302,461.47" transform="translate(-130.5 -314.02)"/><text class="cls-1" transform="translate(119.63 89.67)">0</text><text class="cls-2" transform="translate(93.19 62.15)">0</text><text class="cls-3" transform="translate(115.89 51.72)">1</text><text class="cls-2" transform="translate(134.08 69.09)">1</text><text class="cls-1" transform="translate(114.9 71.68)">1</text><text class="cls-2" transform="translate(88.11 37.59)">1</text><text class="cls-4" transform="translate(137.59 38.67)">0</text><text class="cls-5" transform="translate(239.51 181.94)"><tspan class="cls-6">C<tspan class="cls-7" x="95.55" y="0">ODE</tspan></tspan><tspan x="0" y="142.12">DESIGN</tspan></text><text class="cls-8" transform="translate(639.89 213.61) scale(0.99 1)">+<tspan class="cls-9" x="117.36" y="0"> </tspan></text><path d="M311,548.57" transform="translate(-130.5 -314.02)"/><path d="M198.67,515.4" transform="translate(-130.5 -314.02)"/><path d="M215.68,703.91" transform="translate(-130.5 -314.02)"/><path d="M247.73,649.69" transform="translate(-130.5 -314.02)"/><path d="M257.22,685.63" transform="translate(-130.5 -314.02)"/><path d="M228.85,696.29" transform="translate(-130.5 -314.02)"/><path d="M173.08,500.21" transform="translate(-130.5 -314.02)"/><path d="M191.23,505.61" transform="translate(-130.5 -314.02)"/><path d="M146.61,663.72" transform="translate(-130.5 -314.02)"/></svg>
        </a>
    </div>
    <div class="w-full block items-center justify-center flex-grow flex  md:items-center md:w-auto">
        <div class="text-2xl md:flex-grow text-center md:text-left">
            <ul class="list-none p-0 inline-block">
                <li class="inline-block">
                    <a href="/events" class="text-black hover:text-primary-500 p-1">Events</a>
                </li>
                <li class="inline-block ml-2">
                    <a href="/magazin" class="text-black hover:text-primary-500 p-1">Magazin</a>
                </li>
                <li class="inline-block ml-2">
                    <a href="https://community.code.design" class="text-black hover:text-primary-500 p-1" target="_blank">Community</a>
                </li>
                <li class="inline-block ml-2 mt-6 md:mt-0">
                    <a href="/initiative" class="text-black hover:text-primary-500 p-1">Initiative</a>
                </li>
            </ul>
        </div>
        <div class="hidden md:block">
            @component('_components.button')
                @slot('url', '/initiative/unterstuetzen/')
                Unterstützen
            @endcomponent
        </div>
    </div>
</nav>
