<div class="flex justify-between items-center px-6 py-2 text-center md:text-left text-primary-800 bg-gray-200">
    <p>Willst du Code+Design mitgestalten? <a href="/initiative/eventmanagerin" class="block md:inline underline font-bold text-primary-800 rounded-full">Jobangebot: Event- und Projektmanager*in</a></p>
    <div class="flex" id="social">
        <a href="https://instagram.com/codeunddesign/" target="_blank" class="mr-2"><svg class="stroke-current text-gray-500 hover:text-primary-500 w-6 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.5" y2="6.5"></line></svg></a>

        <a href="https://www.youtube.com/channel/UCuT3xJjPZFqQEEpleHBxVuA/videos" target="_blank"><svg class="stroke-current text-gray-500 hover:text-primary-500 w-6 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-play-circle"><circle cx="12" cy="12" r="10"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg></a>

        <a href="https://www.facebook.com/codeunddesign/" target="_blank"><svg class="stroke-current text-gray-500 hover:text-primary-500 w-6 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a>
    </div>
</div>
