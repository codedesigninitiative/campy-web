<div>
<h2>Häufige Fragen</h2>

<section class="leading-normal">
<details>
        <summary class="text-lg">Was passiert auf einem Camp?</summary>
        <ul class="ml-8 mt-2 list-disc leading-normal text-lg">
                <li>Kreativübungen</li>
                <li>Ideen finden und entwickeln</li>
                <li>Pitch-Training und Ideen-Pitch</li>
                <li>Teamarbeit</li>
                <li>Workshops</li>
                <li>Impulsvorträge</li>
                <li>Präsentationen</li>
                <li>Viele gleichgesinnte Jugendliche, die etwas bewegen wollen!</li>
                <li>Kontakte mit erfahrenen Praktikern und Firmen</li>
            </ul>
</details>
<details>
    <summary class="text-lg">Fragen zur Vorbereitung</summary>
    <details><summary class="text-lg">Welche Vorkenntnisse brauche ich?&nbsp;</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Vorkenntnisse sind nicht notwendig. Wenn du schon eine Idee hast, was du machen m&ouml;chtest, aber noch nicht wei&szlig;t, wie man das umsetzen kann, finden wir das auf dem Event gemeinsam heraus :-)</p>
        </details><details><summary class="text-lg">Wie alt darf mein eigener Laptop sein?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">&Auml;lter als 7 Jahre sollte er nicht sein. Richtwert: Hat der Laptop einen USB3-Anschluss, ist er geeignet</p>
        </details><details><summary class="text-lg">Welches Betriebssystem haben die Leihrechner?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Die Leihrechner laufen meist auf Windows 10. Selten haben wir auch Linux-Rechner. Macs sind in der Leihe leider zu teuer.</p>
        </details><details><summary class="text-lg">Ich habe am Tag eines Events Schule. Kriege ich eine Freistellung?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wir k&ouml;nnen es zusammen versuchen. Versprechen k&ouml;nnen wir nichts. Bei anderen hat es schon geklappt.&nbsp;<a href="http://code.design/kontakt">Schreibe uns</a> bitte einfach dazu.</p>
        </details><details><summary class="text-lg">Was passiert nach der Anmeldung?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Du bekommst eine Einladung in den Code+Design Slack. Slack ist ein Chat. Dar&uuml;ber kommunizieren wir alle wichtigen Programmpunkte auf dem Event und auch die Teams arbeiten damit. Melde dich also bitte sofort an!</p>
        </details><details><summary class="text-lg">Welche Programme kann ich schon mal herunterladen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Firefox oder Chromium als Browser werden ben&ouml;tigt, Visual Studio Code f&uuml;rs Programmieren empfohlen.</p></details>
</details>

<details>
    <summary class="text-lg">Fragen zur Organisation</summary>
    <details><summary class="text-lg">Welches Essen gibt es?</summary>
        <ul class="list-disc pl-4">
        <li>Je nachdem von wann bis wann das Event geht, gibt es um 9 Uhr Fr&uuml;hst&uuml;ck, um 13 Uhr Mittagessen und um 18 Uhr Abendessen.</li>
        <li>Wir versuchen neben den Klassikern die m&ouml;glichst Vielen schmecken vor Allem gesundes und ausgewogenes Essen anzubieten.</li></ul>
        </details><details><summary class="text-lg">Was ist Slack? Warum sollte ich das nutzen?</summary>
        <ul class="list-disc pl-4">
        <li>Slack ist ein Tool mit dem wir vor, auf und nach dem Event miteinander chatten k&ouml;nnen.</li>
        <li>&Uuml;ber <a href="https://code.design/chat">code.design/chat</a> kannst du dich selbst mit deiner E-Mail-Adresse anmelden und dann dem #Community und Event Channel beitreten.</li>
        <li>So verpasst du garantiert nichts und kannst auch danach mit Allen in Kontakt bleiben.</li></ul>
        </details><details><summary class="text-lg">Was mache ich, wenn ich veganes/Halal/etc. Essen ben&ouml;tige?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Das kannst du bei der Anmeldung einfach mit angeben.</p>
        </details><details><summary class="text-lg">Hat das Event einen festen Zeitplan?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Am ersten Tag gibt es einen festen Plan: Erst lernen sich alle kennen, dann stellen alle ihre Ideen vor. Ihr findet Teams. Dann gibt es (EinsteigerInnen-)Workshops. Die Erfahrenen helfen mit und lassen sich individuell coachen. Und dann fangt ihr als Team an zu arbeiten. An den Folgetagen arbeitet ihr vor allem an euren Projekten, es gibt einen weniger festen Plan. Jeden Morgen wird der Tagesplan bekanntgegeben.</p>
        </details><details><summary class="text-lg">Wie finde ich einen &Uuml;bernachtungsplatz?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Leider k&ouml;nnen wir dir keinen Schlafplatz anbieten.</p>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Am Besten fragst Du aber im Slack Channel des Events , ob ein Teilnehmer aus der Event-Stadt noch einen Schlafplatz &uuml;brig hat.</p>
        </details><details><summary class="text-lg">Organisiert Code+Design auch meine &Uuml;bernachtung?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Nein. Du musst dich leider selbst darum k&uuml;mmern. Wir bieten dir an, dich in unserem Chat mit anderen Teilnehmern auszutauschen und euch gegenseitig zu helfen. Leider gibt es immer viel mehr Leute, die eine Unterkunft suchen, als solche, die einen Schlafplatz anbieten. Wir empfehlen deshalb unter &quot;Updates&quot; ein Hostel in der N&auml;he, in dem dann auch viele Teilnehmer unterkommen.</p>
        </details><details><summary class="text-lg">Was sollte ich zum Event mitbringen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Du kannst Deinen eigenen Laptop mitbringen oder dir bei der Anmeldung einen bei uns reservieren.</p>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Ansonsten kannst du auch gerne Spiele f&uuml;r den Entertainment Abend mitbringen.</p>
        </details><details><summary class="text-lg">K&ouml;nnen meine Eltern mich auf dem Event besuchen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Zur Abschlusspr&auml;sentation am letzten Tag kannst du deine Eltern und/Freunde gerne einladen, wenn du zeigen m&ouml;chtest, woran du w&auml;hrend des Events gearbeitet hast.</p>
        </details><details><summary class="text-lg">K&ouml;nnen meine Freunde und Verwandte zur Abschlusspr&auml;sentation kommen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Ja, klar!</p>
        </details><details><summary class="text-lg">Ist es schlimm, wenn ich jeden Tag fr&uuml;her gehen muss oder zwischendurch nicht da sein kann?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Du solltest uns immer Bescheid sagen, wenn du das Event verl&auml;sst.</p>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wenn du mit deinem Team gekl&auml;rt hast, dass du zwischendurch oder abends nicht mehr da sein kannst, ist das kein Problem.</p>
        </details><details><summary class="text-lg">Wie spontan kann ich noch Freunde mitbringen, die auch gerne dabei sein wollen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wenn dir noch jemand einf&auml;llt, solltest du uns am Besten kurz schreiben und wir k&ouml;nnen dir dann sagen, ob da noch ein Platz frei ist. f&uuml;rs Fragen ist es nie zu sp&auml;t ;-)</p></details>
</details>

<details>
    <summary class="text-lg">Auf dem Event</summary>
    <details><summary>Was passiert auf einem Camp?</summary>
        <ul class="ml-4 mt-2 list-disc leading-normal text-lg">
            <li>Kreativübungen</li>
            <li>Ideen finden und entwickeln</li>
            <li>Pitch-Training und Ideen-Pitch</li>
            <li>Teamarbeit</li>
            <li>Workshops</li>
            <li>Impulsvorträge</li>
            <li>Präsentationen</li>
            <li>Viele gleichgesinnte Jugendliche, die etwas bewegen wollen!</li>
            <li>Kontakte mit erfahrenen Praktikern und Firmen</li>
        </ul>
    </details>
    <details><summary class="text-lg">Welche Hardware ist vor Ort verf&uuml;gbar?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wir haben immer Zeichenboards und Raspberry Pi's dabei, f&uuml;r weitere Hardware frage bitte die Orga.</p>
        </details><details><summary class="text-lg">Was ist, wenn es auf dem Event zu laut ist und ich mich nicht konzentrieren kann?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wir versuchen immer R&uuml;ckzugsorte zu schaffen, aber manchmal ist das in der Event Location nicht so m&ouml;glich. Am Besten bringst du Kopfh&ouml;rer, wenn du mal ganz abschalten willst. Daf&uuml;r eignet sich dann auch: <a href="https://calmyleon.com/">Calmy Leon</a></p>
        </details><details><summary class="text-lg">Wann kann ich Pause machen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Du kannst Pause machen wann und wie du m&ouml;chtest.</p>
        </details><details><summary class="text-lg">Was mache ich, wenn ich krank bin oder aus einem anderen wichtigen Grund mal einen Tag nicht kommen kann?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Schreib uns bitte eine Mail, damit wir Bescheid wissen, dass du nicht da bist.</p>
        </details><details><summary class="text-lg">Was mache ich, wenn ich &uuml;berhaupt nicht mit dem Eventverlauf einverstanden bin?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Jeden Jeden Abend machen wir eine Feedback Runde. Hier kannst du anmerken, was dir nicht gef&auml;llt oder was du gerne anders machen m&ouml;chtest. Falls du das nicht in der gro&szlig;en Runde machen willst, kannst du auch einen Zettel in die Feedback Box stecken.</p>
        </details><details><summary class="text-lg">Was mache ich, wenn ich w&auml;hrend des Events krank werde und nicht weitermachen kann?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Schreib uns bitte, damit wir Bescheid wissen und wenn du am letzten Tag bei der Abschlusspr&auml;sentation nicht dabei sein kannst und gerne dein Zertifikat haben m&ouml;chtest.</p></details>
</details>

<details>
    <summary class="text-lg">Betreuung</summary>

    <details><summary class="text-lg">Wer sind die Coaches? Was machen die Coaches?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Die Coaches sind ehrenamtliche Mentoren aus der Branche. Sie unterst&uuml;tzen euch dabei Ideen zu entwickeln, Technologien zu verstehen und Probleme zu l&ouml;sen.</p>
        </details><details><summary class="text-lg">Wer hilft mir, wenn ich nicht weiterkomme?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wenn du Hilfe bei etwas brauchst, dass mit deinem Projekt zu tun hat, kannst du jederzeit einen Coach ansprechen. Wenn du nicht wei&szlig;t, welcher Coach dir weiterhelfen kann, schaust du entweder aufs Kompetenzposter oder fragst die Eventleitung. Bei allen Fragen rund um die Location oder den Event-Ablauf kannst du dich jederzeit an die Eventleitung wenden.</p>
        </details><details><summary class="text-lg">Wen spreche ich an, wenn ich mich unwohl f&uuml;hle?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Spreche einfach einen der Orgas an, den du sympathisch findest. Helfen k&ouml;nnen dir alle!</p></details>
</details>

<details>
    <summary class="text-lg">Ablauf/Inhalt</summary>
    <details><summary class="text-lg">Welche Workshops gibt es?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Die Workshops richten sich immer danach, was f&uuml;r die Projekte auf dem Event gebraucht wird. Ihr k&ouml;nnt euch auch Workshops w&uuml;nschen! Am ersten Tag finden einige Einsteiger-Workshops statt um euch einen &Uuml;berblick und erstes Basis-Wissen zu verschaffen.</p>
        </details><details><summary class="text-lg">Wie funktioniert der Workshop DEVELOP Your Future?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Coaches und Besucher stellen sich zuerst auf der B&uuml;hne vor und erkl&auml;ren kurz was sie machen. Dann verteilen sie sich nach Bereichen in der Location und ihr k&ouml;nnt euch aussuchen zu wem ihr zuerst gehen wollt um eure Fragen zu stellen. Nach zehn Minuten ert&ouml;nt ein Signal und ihr k&ouml;nnt entweder zum n&auml;chsten Tisch weitergehen oder noch bleiben, wenn es gerade zu spannend ist.</p>
        </details><details><summary class="text-lg">Was passiert am Spiele-Abend?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Der Spiele-Abend geht nach dem Abendessen los und kann frei gestaltet werden. Es kann alles gespielt werden von <a href="http://Scribbl.io">Scribbl.io</a> &uuml;ber Stadt, Land, Fluss (mit seeehr anderen Kategorien) oder Werwolf und Konsolenspielen. Karaoke oder Verstecken ist mehr dein Ding? Kein Problem! Beim Spiele-Abend ist alles erlaubt was Spa&szlig; macht und in der Location m&ouml;glich ist.</p>
        </details><details><summary class="text-lg">Wie finde ich heraus, ob ein Workshop f&uuml;r mich sinnvoll ist?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Alle Workshops sind interaktiv angelegt und werden als f&uuml;r Anf&auml;nger geeignet oder nicht angek&uuml;ndigt.</p>
        </details><details><summary class="text-lg">Was ist ein Standup und was passiert dabei?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Ein Standup ist die Gelegenheit um festzustellen was in deinem Team passiert. Jeder bekommt die Gelegenheit zu sagen, was ihn gest&ouml;rt oder gefreut hat und was er sich f&uuml;r den n&auml;chsten Tag vornimmt. Ihr k&ouml;nnt die Standups zum einen in eurer Gruppe nutzen und abends gibt es dann den Standup mit dem gesamten Event. Wenn etwas nicht gut l&auml;uft, wollen wir das gerne wissen, damit das Event noch besser werden kann.</p></details>
</details>

<details>
    <summary class="text-lg">Lernen/Fähigkeiten/MitmachEvent</summary>
    <details><summary class="text-lg">Was ist das Kompetenzposter und wof&uuml;r ist es gut?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Jeder Teilnehmer, jeder Coach und jeder Helfer tragen sich mit Bild und dem was sie gut k&ouml;nnen in das Kompetenzposter ein. Wenn du Hilfe brauchst, oder vielleicht jemand deine Hilfe brauchen k&ouml;nnte, k&ouml;nnt ihr euch &uuml;ber das Kompetenzposter finden.</p>
        </details><details><summary class="text-lg">Wer kann das Kompetenzposter sehen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Das Kompetenzposter ist nur f&uuml;r die Anderen auf dem Event sichtbar und wird niemals &ouml;ffentlich gepostet.</p>
        </details><details><summary class="text-lg">Was ist, wenn ich nicht wei&szlig;, was HTML, Unity und JavaScript sind?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Am ersten Tag stellen wir beim Markt der M&ouml;glichkeiten und in den Einsteiger-Workshops die wichtigsten Technologien vor. Falls da was fehlt, kannst du uns immer fragen. Niemand kennt Alles und niemand wei&szlig; Alles, also nicht sch&uuml;chtern sein.</p>
        </details><details><summary class="text-lg">Was ist die d&uuml;mmste Frage, die ich stellen kann?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Es gibt mit Sicherheit keine dummen Fragen. Dumm w&auml;re nur, wenn du die Gelegenheit verpasst eine Frage zu stellen deren Antwort dir geholfen h&auml;tte.</p>
        </details><details><summary class="text-lg">Was ist das Rubberduck-Prinzip?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Manchmal hilft es das Problem bei dem man nicht weiterkommt jemand anderes zu erkl&auml;ren, um selbst auf die L&ouml;sung zu kommen. Daher haben manche Entwickler eine Badeente (Rubber Duck) auf ihrem Schreibtisch, die ihnen geduldig zuh&ouml;rt, wenn es mal nicht weitergeht :-D.</p>
        </details><details><summary class="text-lg">Was ist, wenn ich schon sehr viel kann und die Workshops zu leicht f&uuml;r mich sind?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Welche Workshops w&uuml;rden dich denn interessieren? Vielleicht k&ouml;nnen wir was passendes f&uuml;r dich finden? Oder vielleicht h&auml;ttest du Lust selbst einen Workshop zu halten? Dabei lernt man immer noch was dazu.</p></details>
</details>

<details>
    <summary class="text-lg">Projekt und Team</summary>
    <details><summary class="text-lg">Was mache ich, wenn ich schon vor dem Event wei&szlig;, was ich f&uuml;r ein Projekt machen will?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Gro&szlig;artig! Du kannst schon vor dem Event &uuml;ber Slack posten, was du vorhast und vielleicht findest du so schon erste Teammitglieder / Mitstreiter f&uuml;r deine Sache!</p>
        </details><details><summary class="text-lg">Muss ich die ganze Zeit im Projekt arbeiten?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Ihr organisiert euch in den Gruppen selbst. Wenn du eine Pause brauchst oder den ein oder anderen Extra-Workshop mitnehmen willst, solltest du das mit deinen Teammitgliedern besprechen.</p>
        </details><details><summary class="text-lg">Dauert das Projekt die ganze Zeit?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Am ersten Tag finden Einstiegsworkshops statt und die Projekte und Gruppen finden sich. Danach l&auml;uft die Gruppenarbeit parallel zu den Workshops weiter. Aber es ist euch &uuml;berlassen, wieviel Zeit ihr investiert. Uns ist wichtig, dass ihr das Beste aus eurer Zeit auf dem Event macht!</p>
        </details><details><summary class="text-lg">Was ist mit dem Projekt, wenn ein Workshop l&auml;uft?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Die Projektarbeit l&auml;uft parallel zu den Workshops. Stimmt euch im Team ab, damit an niemandem die ganze Arbeit h&auml;ngenbleibt und niemand den Anschluss verliert nur weil er beim Workshop war.</p>
        </details><details><summary class="text-lg">Wie finde ich heraus, welche Technologie f&uuml;r mein Projekt wichtig ist?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Nach dem Markt der M&ouml;glichkeiten, solltest du schon eine ungef&auml;hre Idee haben. Solltest du dir immer noch unsicher sein, kannst du bei deinem Ideen Pitch darauf hinweisen, dass du dir bei der Suche nach der richtigen Technologie Hilfe w&uuml;nschst.</p>
        </details><details><summary class="text-lg">Bis wann kann ich das Team wechseln?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Auf dem Event gilt die Regel: Abstimmung mit den F&uuml;&szlig;en. Das gilt von Anfang bis Ende. Wenn du also am 3. Tag feststellst, dass du in deiner Gruppe ungl&uuml;cklich bist oder lieber in ein anderes Team wechseln m&ouml;chtest, kannst du das machen. Fairness sollte aber eine Rolle spielen, also lass bitte keine Gruppe zur&uuml;ck, die ohne dich nicht weitermachen kann ohne eine Alternative f&uuml;r sie zu suchen.</p>
        </details><details><summary class="text-lg">Was ist, wenn ich keine eigene Projektidee habe?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Dann kannst du dich am ersten Tag bei den Ideen Pitches &uuml;berzeugen lassen, welchem Team du beitreten m&ouml;chtest.</p>
        </details><details><summary class="text-lg">Wie gro&szlig; sollten/m&uuml;ssen die Gruppen sein?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Im Idealfall ist eure Gruppe zwischen 3-8 Personen gro&szlig;. Es bleibt euch &uuml;berlassen ob ihr gr&ouml;&szlig;ere oder kleinere Gruppen habt, aber erfahrungsgem&auml;&szlig; sind je nach Ausma&szlig; und Komplexit&auml;t des Projektes diese Gruppengr&ouml;&szlig;en sinnvoll.</p>
        </details><details><summary class="text-lg">Was ist, wenn ich allein an einem Projekt arbeiten m&ouml;chte?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Dann verpasst du viele Chancen die das Event zu bieten hat. Im Team kommt man weiter und es macht auch viel mehr Spa&szlig;.</p></details>
</details>

<details>
    <summary class="text-lg">Nach dem Event</summary>
    <details><summary class="text-lg">Wie kann ich die Initiative unterst&uuml;tzen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Du kannst uns als Botschafter unterst&uuml;tzen und uns helfen Code+Design gr&ouml;&szlig;er und besser zu machen. Wir freuen uns au&szlig;erdem immer &uuml;ber ehrliches und konstruktives Feedback und Verbesserungsvorschl&auml;ge. Die Community hat Code+Design zu dem gemacht was es ist und unsere Philosophie und Kultur mitgestaltet.</p>
        </details><details><summary class="text-lg">Was kann ich nach dem Event machen?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Wenn du an deinem Projekt weiterarbeiten willst und nicht weiterkommst, kannst du die Community fragen. Bei Slack bleibst du ausserdem auf dem Laufenden was bei Code+Design als n&auml;chstes passiert.</p>
        </details><details><summary class="text-lg">Ich interessiere mich f&uuml;r ein Praktikum. Wie finde ich eines?</summary>
        <p class="ml-3 mt-1 mb-3 italic text-lg">Du wei&szlig;t schon in welchem Bereich du Praktikum machen willst? Super, sprich einfach einen der Coaches aus der Branche an, ob er dir die n&auml;chsten Schritte empfehlen kann. Du wei&szlig;t noch nicht, wo es hingehen soll? Beim Berufs-Speed-Dating hast du die Chance mehr zu erfahren und alle deine Fragen loszuwerden.</p></details>
</details>

</section>

</div>
