---
extends: _layouts.en_coach
section: content

title: Join as a Coach - Questions and Answers
---

### 🇬🇧🇩🇪 Do I need to speak German to coach at the camps?
The camp is 95% in German. Nonetheless we have had english-only coaches with great success. This FAQ is also available in [German](/coach/faq).

### Why should I join as a coach?
- Help the next generation of techies!
- We would have wished for such a camp earlier, there was no offer for digitally interested people...
- The teenagers are super grateful for the support
- Interesting projects: The young people have ideas that refresh them
- You will meet other dedicated coaches from different areas

### Is the coaching paid for?
90% of our coaches are volunteers because they want to give back to the community. Rarely can we cover the travel and accommodation costs for coaches.

### What is my role as a coach?
- As a team coach you are the first contact person for technical and/or creative questions.
- You are not obliged to solve the problem for your team - it is more about showing them ways and means how they can acquire knowledge themselves.
- We invite you to actively participate in the Code+Design Camp. If you have suggestions for improving the event: Make it your camp!

### Do I have to lead a workshop?
No, of course it's voluntary.

### Do I have to supervise a group?
Only if you think you like it and feel like it.

### Can I go away for a moment in between?
Sure, but please write to the participants on Slack when you will be back.

### Will I be reimbursed for travel expenses and accommodation?
We have a very small budget for these expenses. Talk to us if you need support.

### Can I recruit participants for my company?
The camp is not a promotional event. The participants should first have a great camp and be able to try out and learn. If they talk to you and ask for an internship, you can of course offer it.

### Do I have to be there from start to finish?
Of course not. You are a volunteer, you come when you can. You can indicate this in a very differentiated way when you register.

### Who do I address for Orga questions?
Before the event: hello@code.design; At the event: The camp management introduces itself at the beginning.

### Which programming languages are used?
There are no fixed programming languages. The groups choose the technologies themselves. Often used: Python, PHP, JavaScript, Java (for gaming and apps). The young people also orient themselves towards the coaches and their skills.

### How much do the teams manage in a few days?
The goal is that every participant goes home with a successful result - and with a visible product. Whether it is a clickable prototype or a semi-finished app, it makes no difference. There is also no competition between the project groups, on the camp only the joy of Tech should be conveyed.

### How do the workshops work?
- Young people discover something new in workshops
- In workshops, the young people create a complete product: e.g. a mini app, a web page, a prototype of a web app, etc.
- Workshops are always goal-oriented: at the beginning the young people see what the product looks like at the end.
- The young people use a step-by-step tutorial (text/video), help each other and ask the coach about problems.
- Workshops will take place during the camp parallel to the project work.
- The young people bring along: no previous knowledge but endless motivation
- The workshops last 1-1.5 hours.
- Popular topics: Interface design with Figma, website with HTML/CSS, app with Thunkable, board with soldering iron
- Workshops only use software that runs in the browser
- As a coach you bring along a (self-created or finished) workshop, are responsible for the framework (time, space, well-being of the participants) and ensure the learning success for all.
- Workshops can also have sequels and become more challenging - they are subject to the same requirements as all other workshops.

## What makes good coaches?
- Ask questions and don't have an immediate answer to everything.
- Listen
- Encourage young people to find their own solutions
- Are interested in more than their profession
- Are interested in the learning path of the participants (also beyond the technical and the camp)?

### How much experience do the participants have?
50% are total beginners, 40% have previous knowledge. 10% are upper cracks.

### How many girls are in the camp?
Always at least 30%.


### How does learning succeed at the camp?
- Assume that everyone you coach has zero knowledge but infinite intelligence.
- Be patient with each participant's personality, speed and manner.
- We as coaches are role models in everything we do.
- Encourage learners to be independent and experiment.
- We do not discuss which programming language, methods or technologies are "better".
- The learner's keyboard is made of lava. (You lose the learner when you take over the keyboard).

### What is Job Speed Dating?
One goal of the Code+Design initiative is to connect young people with the digital world of work. Many young people would like to do internships or student jobs and have no contacts at all. With Job Speed Dating, all coaches who can and want to arrange contacts and jobs sit at tables that represent areas (e.g. design, backend, server, hardware, games). The young people sit down at the tables and talk to the coaches and the coaches try to establish the right contacts for the job enquiries.

### Will I get paid as a coach?
Code+Design is a small non-profit organization with the sole goal of getting young people excited about digital professions. We are not allowed to make any profits and all our activities must be geared to the purpose of the association. The events we organize are very expensive and time-consuming. For the organisation of the events and the office we also need a head official, for whom we have to pay a salary. Since we do not receive any subsidies from the federal government, the federal states or local authorities, we are dependent on sponsoring by companies to cover our costs.

### How does vocational orientation work?
- On one of the last evenings we have a job orientation session, as most young people have no idea what great job opportunities they can expect.
- The vocational orientation consists of 2 parts: 1 podium round and 1 small group. On the podium there are 5 coaches, 1 moderated team player. He asks 4 questions, each question has 1 minute for an answer:
- Who are you? What are you doing? Where?
- What does your daily work look like?
- If you were 17 today and started, what would you learn and why?
- How do you learn / educate yourself?
- In the small groups (5-7) the young people can ask questions about everyday work, about training/study and about the industry (games, apps, etc.). For the podium we choose 5 coaches who are as different as possible. Each coach can participate in the small groups. Please click "yes" if you want to participate in one or both formats. If you change your mind, you can also send us an e-mail.

### What are the rules for coaches?
- You are role models, please behave like role models!
- Say hello when you come. Bye, if you go.
- No alcohol: Many teenagers are not allowed either and we don't want any privileges at the camp.
