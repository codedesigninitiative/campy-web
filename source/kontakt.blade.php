@extends('_layouts.master')

@section('body')
        <p>
            Das Kontaktformular ist gerade nicht verfügbar.<br>
            Bitte sende eine E-Mail an: <a class="text-primary-500 font-semibold hover:text-primary-700" href="mailto:hello@code.design">hello@code.design</a><br>
            Wenn du jemand spezifischen kontaktieren willst, dann schaue auf unserer <a class="text-primary-500 font-semibold hover:text-primary-700" href="/initiative">Team Seite</a> vorbei.
        </p>
        <!--div id="contact-form"></div>
        <script src="{{ $page->appUrl }}js/contact.js"></script>
        <script>
            window.onload = () => {
                mountContact('#contact-form')
            }
        </script-->
@endsection

@section('title')
Kontakt
@endsection
