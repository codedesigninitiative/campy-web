---
extends: _layouts.blogs
caption: "Schirmherrin OB Henriette Reker"
image: /img/blog/191106-reker.jpg
image_source: "Wikipedia"
published_at: 2019-11-06
lead: "Frau Oberbürgermeisterin Henriette unterstützt das von jungen Menschen organisierte Code+Design Camp in Köln 2019."
---
Code+Design will Freude am kreativen Programmieren und Designen vermitteln und veranstaltet dafür in ganz Deutschland Code+Design Camps – im sogenannten Hackathon-Format – für bis zu 85 Jugendliche im Alter von 14 bis 22 Jahren.

Vom Freitag 08. bis zum Sonntag 10. November 2019 findet ein Code+Design Camp in Köln statt. Schirmherrin der Veranstaltung ist die Oberbürgermeisterin Henriette Reker.

Mit diesem Camp möchten die Veranstalter motivierten Jugendlichen die Chance geben, sich mit digitalen Technologien vertraut zu machen. Dafür werden über einen Zeitraum von zweieinhalb intensiven Tagen sowohl spannende Hard- und Software-Projekte bearbeitet, als auch Programmier- und Design-Grundlagen durch interaktive Workshops vermittelt.
Unterstützt werden die Teilnehmer von erfahrenen Software-Entwicklern, Designern und Produktmanagern.
Auch steht die Berufsorientierung im Mittelpunkt: Die Jugendlichen kommen mit den Praktikern ins Gespräch und können ihre Fragen und Erwartungen an Berufe der Branche klären.

## Vorberichterstattung und Einladung
Gerne möchten wir Sie hiermit einladen, über unser Camp zu berichten.
Für die Vorberichterstattung steht Ihnen Johannes Kaufmann Rede und Antwort – er ist ehemaliger Teilnehmer von Code+Design und jetzt studentischer Vorstand.

Auch würden wir Sie zusätzlich auch gerne vor Ort begrüßen.

Unsere Abschlussveranstaltung findet am Sonntag, den 10. November 2019, um 15:00 Uhr in den Räumen der Ambient Innovation GmbH, Oskar-Jäger-Straße 173, 50825 Köln, statt.

Wir sind Ihnen sehr dankbar, wenn Sie Ihre Teilnahme vorab kurz bestätigen. Gerne können Sie auch nach Absprache zu einem anderen Zeitpunkt vorbeikommen.

## Pressekontakt
Code+Design e. V.: Johannes Kaufmann, studentischer Vorstand, johannes@code.design, +49 176 2444 0054

## Auf einen Blick

- 8.  bis 10. November 2019, Freitag 16:30 - Sonntag 18:00
- Veranstaltungsort: Ambient Innovation GmbH, Oskar-Jäger-Straße 173, 50825 Köln
- für Jugendliche zwischen 14 und 22 Jahren
- Hackathon-Format mit starker Eigenorganisation der Teilnehmer
- Projekte im Bereich Hard- und Software sowie App-Entwicklung
- Berufsorientierung im Bereich Informatik und Design
- Unkostenbeitrag 50 € (+ Leihlaptop, falls benötigt), kostenfreie Teilnahme für alle, die sich Gebühren nicht leisten können
- Unterstützung durch Partner: Unitymedia, Ambient Innovation GmbH, Stadt Köln

## Weitere Materialien:

- Fotoalbum der bisherigen Code+Design Camps: https://www.flickr.com/photos/codeunddesign
- 2-Minuten-Video des Camps in Berlin: https://www.youtube.com/watch?v=xGk1PpIbisU+
- Weitere Informationen: https://code.design/

## Unterstützer
Das Code+Design Camp Köln 2019 wird von folgenden Partnern unterstützt:

- Unitymedia
- Ambient Innovation GmbH
- Köln Business
