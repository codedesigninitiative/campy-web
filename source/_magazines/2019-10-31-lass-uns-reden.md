---
extends: _layouts.blogs
caption: "Lass uns reden"
image: /img/blog/191031-amazon-echo.jpg
image_source: "Amazon"
author: "Justus Zenker"
published_at: 2019-10-31
lead: "Es ist nur noch eine Frage der Zeit, bis wir uns mit Maschinen so unterhalten wie mit Menschen. Der Schlüssel zum gegenseitigen Verständnis ist die Programmierung von künstlicher Intelligenz."
---

Wenn Menschen sich unterhalten, dann finden Spracherkennung, Inhalts­analyse und Informationsverarbei­tung simultan statt. Während unser Gegenüber noch vom Urlaub spricht, verknüpft unser Gehirn bereits Informationen zu Finanzen, Urlaubsplanung oder -erinnerungen und stellt uns damit ein Potpourri möglicher Rückfragen zur Verfügung. Bei Sprachassistenzsystemen ist der Prozess linear – aber nicht unbedingt weniger komplex.


<p class="mt-4">Die Geschichte der Spracherkennung beginnt 1952. Bell Laboratories legt mit dem Programm Audrey, das gesprochene Zahlen am Telefon erkennt, den Grundstein. Zehn Jahre später feiert IBM den Durchbruch mit Shoebox, das ganze 16 englische Wörter versteht. Aus dem Spracherkennungsprogramm des US-Ver­teidigungsministeriums geht weitere zehn Jahre später Harpy hervor, das mit 1.011 Wörtern über den Wortschatz eines Dreijährigen verfügt. Die im vorigen Jahrtausend eingesetzte Sprach­erkennungsprogramme basierten auf der Erkennung von festen Ton- und Wortmustern. Ihre jüngeren Brüder und Schwestern hingegen arbeiten mit statistischen Modellen und Analysen. Diese liefern Wahrscheinlichkeiten für verschiedene Wörter, wobei dasjenige mit der höchsten Wahrscheinlichkeit gewählt wird.</p>

<p class="mt-4">Ein weiterer Faktor für den Durchbruch der Sprachtechnik ist die Verlagerung der Analyse in die Cloud auf leistungsfähige Hardware. Waren früher noch ruhige Arbeitsplätze und Headsets für eine passable Spracherkennung notwendig, funktioniert das Gespräch mit der künstlichen Intelligenz inzwischen auch im Auto oder im Café.</p>

<p class="mt-4">Als Geburtsstunde für virtuelle Assistenz dürfte die Gründung des Projekts CALO (Cognitive Assistant that Learns and Organizes) gelten, das im Mai 2003 am Stanford Research Institute startete. „Ziel des Projekts ist es, eine kognitive Software zu entwickeln, die logisch denkt, aus Erfahrung lernen kann, macht, was man ihr sagt, Entscheidungen begründet und sicher auf überraschende Fragen antwortet“, erklären die Entwickler\*innen. Außerdem soll die Software durch stetige Interaktion mit den Anwender*innen schlauer werden.</p>

<p class="mt-4">Aus CALO ging 2007 das Spin-off Siri hervor. Drei Jahre später startete Siri als App für Apples iOS. Siri-Mitbegründer Dag Kittlaus hatte schon damals eine sehr konkrete Vorstellung von der sprachgesteuerten Zukunft: „Es wird einen virtuellen Assistenten geben, der alles für dich macht – Flüge buchen, Kleidung kaufen – und dein Leben in jeder Hinsicht plant.“</p>
