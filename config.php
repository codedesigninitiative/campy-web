<?php

return [
    'fulldate' => function ($shortdate) {
        return date('l d.m.Y', $shortdate);
    },
    'baseUrl' => 'https://code.design/',
    'appUrl' => 'https://app.code.design/',
    'production' => false,
    'collections' => [
        'events' => [
            'path' => 'events/{-slug}/{date_start|ym}',
            'sort' => 'date_start',
        ],
        'magazines' => [
            'path' => 'magazin/{-filename}',
            'sort' => '-published_at',
            'author' => 'Code+Design Initiative',
        ],
        'team' => [
            'path' => 'initiative/{-lastname}',
            'sort' => 'lastname',
        ],
        'partners' => [
            'path' => 'partner/{-name}',
            'hasTier' => function ($page, $tier) {
                return collect(explode(',', $page->tier))->contains($tier);
            },
            'hasEvent' => function ($page, $event) {
                return collect(explode(',', $page->events))->contains($event);
            },
            'sort' => '-strength',
        ],
        'testimonials' => [
            'path' => 'stimmen/{published_at|ym}/{-slug}',
            'sort' => '-published_at',
        ],
        'updates' => [
            'path' => 'updates/{camp}/{published_at|ym}/{-slug}',
            'sort' => '-published_at',
        ],
    ],
];
